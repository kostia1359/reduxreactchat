import {combineReducers} from "redux";
import chat from "../chat/chatReducer";
import messageEditor from "../messageEditorModal/editReducer";

const rootReducer = combineReducers({
    chat,
    messageEditor
});

export default rootReducer;
