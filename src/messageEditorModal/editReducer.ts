import {editMessageState} from "../types/editMessageState";
import {actionTypes, IEditorActions} from "./editActionTypes";

const initialState:editMessageState={
    id:'',
    isShown:false
}

export default function (state=initialState, action:IEditorActions):editMessageState {
    switch (action.type) {
        case actionTypes.SET_CURRENT_MESSAGE_ID:{
            const {id}=action.payload;
            return {
                ...state,
                id
            }
        }
        case actionTypes.DROP_CURRENT_MESSAGE_ID:{
            return {
                ...state,
                id:''
            }
        }
        case actionTypes.SHOW_EDITOR:{
            return {
                ...state,
                isShown:true
            }
        }
        case actionTypes.HIDE_EDITOR:{
            return {
                ...state,
                isShown:false
            }
        }
        default:
            return state
    }
}
