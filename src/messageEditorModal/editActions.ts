import {actionTypes, IDropID, IHideEditor, ISetID, IShowEditor} from "./editActionTypes";

export const dropMessageId = (): IDropID => ({
    type: actionTypes.DROP_CURRENT_MESSAGE_ID,
});

export const setMessageId=(id:string):ISetID=>({
    type: actionTypes.SET_CURRENT_MESSAGE_ID,
    payload:{
        id
    }
});

export const showModal=():IShowEditor=>({
    type: actionTypes.SHOW_EDITOR
})

export const hideModal=():IHideEditor=>({
    type: actionTypes.HIDE_EDITOR
})
