export enum actionTypes {
    DROP_CURRENT_MESSAGE_ID = 'DROP_CURRENT_MESSAGE_ID',
    SET_CURRENT_MESSAGE_ID = 'SET_CURRENT_MESSAGE_ID',
    SHOW_EDITOR = 'SHOW_EDITOR',
    HIDE_EDITOR = 'HIDE_EDITOR'
}

export interface IDropID {
    type: typeof actionTypes.DROP_CURRENT_MESSAGE_ID,
}

export interface ISetID {
    type: typeof actionTypes.SET_CURRENT_MESSAGE_ID,
    payload: { id: string }
}

export interface IShowEditor {
    type: typeof actionTypes.SHOW_EDITOR,
}

export interface IHideEditor {
    type: typeof actionTypes.HIDE_EDITOR,
}

export type IEditorActions = IHideEditor | IShowEditor | ISetID | IDropID;
