import React, {ChangeEvent, Component} from "react";
import {connect} from "react-redux";
import * as actions from "./editActions";
import {editMessage} from "../chat/actions";
import "./messageEditor.css"
import {chatSystemState} from "../types/chatState";
import {editMessageState} from "../types/editMessageState";
import {IMessage} from "../types/message";

interface IProps {
    hideModal: Function,
    showModal: Function,
    dropMessageId: Function,
    editMessage: Function,
    messages?: Array<IMessage>,
    isShown?: boolean,
    id?: string
}

class MessageEditor extends Component<IProps, { message: string }> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            message: ""
        }
        this.onChange.bind(this);
    }

    componentWillReceiveProps(nextProps: IProps) {
        if (nextProps.id !== this.props.id) {
            const message = this.props.messages!.find(message => message.id === nextProps.id)!.text;

            this.setState({
                message
            })
        }
    }

    onChange(event: ChangeEvent<HTMLInputElement>, fieldName: 'message') {
        const value = event.target.value;
        this.setState({
            [fieldName]: value
        })
    }

    onCancel() {
        this.props.hideModal();
        this.setState({
            message: this.props.messages!.find(message => message.id === this.props.id)!.text
        })
    }

    onEdit() {
        this.props.editMessage(this.props.id, this.state.message);
        this.props.hideModal();
    }

    render() {
        if (!this.props.isShown) return null;
        return (
            <div className={"EditorModal"}>
                <input value={this.state.message} className={"editorField"}
                       onChange={e => this.onChange(e, 'message')}/>
                <button className={"editButton"} onClick={() => this.onEdit()}>Edit</button>
                <button className={"cancelButton"} onClick={() => this.onCancel()}>Cancel</button>
            </div>
        )
    }
}

const mapStateToProps = (state: {
    chat: chatSystemState,
    messageEditor: editMessageState
}) => {
    return {
        messages: state.chat.messages,
        isShown: state.messageEditor.isShown,
        id: state.messageEditor.id
    }
}

const mapDispatchToProps = {
    ...actions,
    editMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditor);
