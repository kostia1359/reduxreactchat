import React, {Component} from "react";
import PropTypes from 'prop-types';
import {IMessage} from "../types/message";
import ForeignMessage from "../message/foreignMessage";
import OwnMessage from "../message/ownMessage";
import "./messageList.css"
import {MILLISECONDS_IN_DAY} from "../heplers/constants";
import MessageDate from "./MessageDate";
import {chatSystemState} from "../types/chatState";
import {connect} from "react-redux";
import MessageEditor from "../messageEditorModal/messageEditor";

interface IProps {
    messages?: Array<IMessage>
}

class MessageList extends Component<IProps, any> {
    constructor(props: IProps) {
        super(props);
    }

    static propTypes = {
        messages: PropTypes.array,
        editMessage: PropTypes.func,
        deleteMessage: PropTypes.func,
    }

    generateDate(message1: IMessage, message2?: IMessage) {
        const dateMessage1 = new Date(message1.createdAt)
        if (message2 === undefined) {
            return (
                <MessageDate date={dateMessage1} />
            )
        }
        const dateMessage2 = new Date(message2.createdAt);

        if (dateMessage2.getTime() - dateMessage1.getTime() > MILLISECONDS_IN_DAY * 2
            || dateMessage1.getDate() !== dateMessage2.getDate()) {
            return (
                <MessageDate date={dateMessage1} />
            )
        }

    }

    getMessage=(message: IMessage, index: number)=> {
        if (message.userId === "0") {
            return <div key={index}>
                {this.generateDate(this.props.messages![index], this.props.messages![index-1])}
                <OwnMessage id={message.id}/>
            </div>
        }
        return (
            <div key={index}>
                {this.generateDate(this.props.messages![index], this.props.messages![index-1])}
                <ForeignMessage id={message.id} />
            </div>
        )
    }

    render() {
        return (
            <div className={"messagesList"}>
                <MessageEditor />
                {this.props.messages!.map(this.getMessage)}
            </div>
        );
    }
}

const mapStateToProps=(state:{chat:chatSystemState})=>{
    return{
        messages:state.chat.messages
    }
}

export default connect(mapStateToProps,null)(MessageList);
