import {IMessage} from "./message";
import {actionTypes} from "../chat/actionTypes";

export interface chatSystemState {
    messages: Array<IMessage>,
    isDataLoaded: boolean,
    messagesAmount: number,
    lastMessage: number,
    userAmount: number,
    chatName:string
}

