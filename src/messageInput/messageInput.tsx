import React, {ChangeEvent, Component} from "react";
import {connect} from "react-redux";
import {addMessage} from "../chat/actions";
import "./messageInput.css"
import {chatSystemState} from "../types/chatState";
import {IMessage} from "../types/message";
import {setMessageId, showModal} from "../messageEditorModal/editActions";

interface IProps {
    addMessage: Function,
    setMessageId: Function,
    showModal: Function,
    messages?: Array<IMessage>
}

class MessageInput extends Component<IProps, { message: string }> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            message: ""
        }
        this.onChange.bind(this);
    }

    onArrowDown(event: React.KeyboardEvent<HTMLInputElement>) {
        if (event.key === 'ArrowUp') {
            const ownMessages = this.props.messages!.filter(message => message.userId === '0');

            if (ownMessages.length !== 0) {
                const id = ownMessages[ownMessages.length - 1].id;
                this.props.setMessageId(id);
                this.props.showModal();
            }
        }
    }

    onChange(event: ChangeEvent<HTMLInputElement>, fieldName: 'message') {
        const value = event.target.value;
        this.setState({
            [fieldName]: value
        })
    }

    onClick() {
        const messageValue = this.state.message;
        if (messageValue.trim().length === 0) return;
        this.props.addMessage(messageValue);
        this.setState({
            message: ""
        })
    }

    render() {
        return (
            <div className={"messageInput"}>
                <input value={this.state.message} className={"messageInputField"}
                       onKeyDown={(event) => this.onArrowDown(event)}
                       onChange={e => this.onChange(e, 'message')}/>
                <button className={"messageInputButton"} onClick={() => this.onClick()}>Send</button>
            </div>
        )
    }
}

const mapDispatchToProps = {
    addMessage,
    showModal,
    setMessageId
}

const mapStateToProps = (state: { chat: chatSystemState }) => {
    return {
        messages: state.chat.messages
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
