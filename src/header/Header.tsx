import React, {Component} from "react";
import {getDayName, getTimeString} from "../heplers/timeHelpers";
import './header.css';
import {connect} from "react-redux";
import {chatSystemState} from "../types/chatState";


interface IProps {
    name?: string,
    usersAmount?: number,
    messagesAmount?: number,
    lastMessage?: number
}


class Header extends Component<IProps, any> {
    render() {
        const data = this.props;
        const lastMessageDate = (function () {
            const date = new Date(data.lastMessage!);

            return `${getDayName(date)} at ${getTimeString(date)}`;
        })()
        return (
            <div className={"header"}>
                <div className={"header--chatName"}>{data.name}</div>
                <div className={"header--chatUsers"}>{`${data.usersAmount} participants`}</div>
                <div className={"header--chatMessages"}>{`${data.messagesAmount} messages`}</div>
                <div className={"header--chatLastMessage"}>{lastMessageDate}</div>
            </div>

        );
    }
}

const mapStateToProps = (state: { chat: chatSystemState }): IProps => {
    return {
        usersAmount: state.chat.userAmount,
        name: state.chat.chatName,
        messagesAmount: state.chat.messagesAmount,
        lastMessage: state.chat.lastMessage
    }
}


export default connect(mapStateToProps, null)(Header);
