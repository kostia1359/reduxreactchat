import {chatSystemState} from "../types/chatState";

export enum actionTypes {
    SET_INITIAL_DATA = 'SET_INITIAL_DATA',
    ADD_MESSAGE = 'ADD_MESSAGE',
    DELETE_MESSAGE = 'DELETE_MESSAGE',
    EDIT_MESSAGE = 'EDIT_MESSAGE'
}

export interface IEditMessage {
    type: typeof actionTypes.EDIT_MESSAGE,
    payload: { text: string; id: string }
}

export interface IDeleteMessage {
    type: typeof actionTypes.DELETE_MESSAGE,
    payload: { id: string }
}

export interface IAddMessage {
    type: typeof actionTypes.ADD_MESSAGE,
    payload: { text: string }
}

export interface ISetInitialData {
    type: typeof actionTypes.SET_INITIAL_DATA,
    payload: {
        state:chatSystemState
    }
}

export type IChatActions = IAddMessage | IDeleteMessage | ISetInitialData | IEditMessage;

