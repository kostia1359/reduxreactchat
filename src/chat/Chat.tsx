import React, {Component} from "react";
import Header from "../header/Header";
import {IMessage} from "../types/message";
import MessageList from "../messageList/messageList";
import MessageInput from "../messageInput/messageInput";
import source from '../source.gif';
import {chatSystemState} from "../types/chatState";
import {chatName} from "../chatConfig";
import {setInitialData} from './actions';
import {connect} from "react-redux";
import {data} from "../mockData";


async function getData(): Promise<chatSystemState> {
    const userIDs = new Set<string>();

    const messages = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json').then(res => res.json())
        .catch(error=> {
            console.log(error);
            return  data;
        })
        .then((messages: Array<IMessage>) =>
            messages.sort((message1, message2) => {
                const date1 = new Date(message1.createdAt);
                const date2 = new Date(message2.createdAt);

                return date1.getTime() - date2.getTime();
            })
        );
    messages.forEach(message => userIDs.add(message.userId));


    const lastMessage = new Date(messages[messages.length - 1].createdAt).getTime();
    return {
        messages,
        messagesAmount: messages.length,
        lastMessage,
        isDataLoaded: true,
        userAmount: userIDs.size + 1,
        chatName
    }
}

interface IProps {
    setInitialData:Function,
    isDataLoaded?:Boolean
}

class Chat extends Component<IProps, never> {
    constructor(props: any) {
        super(props);
    }

    async componentDidMount() {
        const {messages, messagesAmount, lastMessage, isDataLoaded, userAmount} = await getData();

        this.props.setInitialData({
                messages, messagesAmount, lastMessage, isDataLoaded, userAmount
            });

    }

    render() {
        if (!this.props.isDataLoaded) {
            return (
                <img className={"preloader"} src={source} alt="preloader"/>
            )
        }
        return (
            <div>
                <Header />
                <MessageList />
                <MessageInput />
            </div>
        );
    }
}

const mapDispatchToProps={
    setInitialData
}

const mapStateToProps=(state:{chat:chatSystemState})=>{
    return{
        isDataLoaded:state.chat.isDataLoaded
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
