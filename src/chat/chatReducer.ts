import {IMessage} from "../types/message";
import {chatSystemState} from "../types/chatState";
import {actionTypes, IChatActions} from "./actionTypes";

const initialState: chatSystemState = {
    isDataLoaded: false,
    lastMessage: 0,
    messages: [],
    messagesAmount: 0,
    userAmount: 0,
    chatName: ""
};


export default function (state = initialState, action: IChatActions): chatSystemState {
    switch (action.type) {
        case actionTypes.SET_INITIAL_DATA: {
            const {messages, messagesAmount, lastMessage, isDataLoaded, userAmount, chatName} = action.payload.state;

            return {
                messages,
                messagesAmount,
                lastMessage,
                isDataLoaded,
                userAmount,
                chatName
            };
        }
        case actionTypes.ADD_MESSAGE: {
            const {text} = action.payload;
            const message: IMessage = {
                user: "you",
                userId: "0",
                id: Date.now().toString(),
                editedAt: "",
                createdAt: new Date().toISOString(),
                avatar: "",
                text,
            }

            const messagesAmount = state.messagesAmount + 1;
            const lastMessage = Date.now();

            const messages = state.messages.slice();
            messages.push(message);
            return {...state, messages, messagesAmount, lastMessage};
        }
        case actionTypes.DELETE_MESSAGE: {
            const {id} = action.payload;
            const messages = state.messages.filter(message => message.id !== id);
            const messagesAmount = state.messagesAmount - 1;
            const lastMessage = new Date(messages[messages.length - 1].createdAt).getTime();


            return {...state, messages, messagesAmount, lastMessage};
        }
        case actionTypes.EDIT_MESSAGE: {
            const {id, text} = action.payload;
            const messages = state.messages.map(message => {
                if (message.id === id) {
                    message.text = text;
                    message.editedAt = new Date().toISOString();
                }
                return message;
            });

            return {...state, messages};
        }
        default: {
            return state;
        }

    }
}
