import {actionTypes, IAddMessage, IDeleteMessage, IEditMessage, ISetInitialData} from "./actionTypes";
import {chatSystemState} from "../types/chatState";


export const editMessage = (id: string, text: string): IEditMessage => ({
    type: actionTypes.EDIT_MESSAGE,
    payload: {
        text, id
    }
});

export const deleteMessage = (id:string):IDeleteMessage=>({
    type: actionTypes.DELETE_MESSAGE,
    payload:{
        id
    }
})

export const addMessage=(text:string):IAddMessage=>({
    type:actionTypes.ADD_MESSAGE,
    payload:{
        text
    }
})

export const setInitialData=(state:chatSystemState):ISetInitialData=>({
    type: actionTypes.SET_INITIAL_DATA,
    payload:{
        state
    }
})
