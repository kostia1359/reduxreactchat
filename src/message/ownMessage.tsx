import React, {Component} from "react";
import {IMessage} from "../types/message";
import {getTimeString} from "../heplers/timeHelpers";
import {deleteMessage} from "../chat/actions";
import {connect} from "react-redux";
import {showModal, setMessageId} from "../messageEditorModal/editActions";
import "./ownMessage.css"
import {chatSystemState} from "../types/chatState";

interface IProps {
    id: string,
    messages?: Array<IMessage>,
    showModal: Function,
    setMessageId: Function,
    deleteMessage: Function
}

class OwnMessage extends Component<IProps, { isFocused: boolean }> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            isFocused: false
        }
    }

    onEditClick(id: string) {
        this.props.setMessageId(id);
        this.props.showModal();
    }

    showEdit() {
        this.setState({
            isFocused: true
        })
    }

    hideEdit() {
        this.setState({
            isFocused: false
        })
    }

    render() {
        const message = this.props.messages!.find(message => message.id === this.props.id)!;
        const time = new Date(message.createdAt);
        const timeString = getTimeString(time);

        return (
            <div className={"message ownMessage"}>
                <div className={"messageInfo"} onMouseEnter={() => this.showEdit()}
                     onMouseLeave={() => this.hideEdit()}>
                    <div className={"messageDate"}>{timeString}</div>
                    <div className={"messageText"}>{message.text}</div>
                    <div className={"messageFooter"}>
                        <div className={"messageFooterObject"}>
                            <button className={"editButton"} hidden={!this.state.isFocused}
                                    onClick={() => this.onEditClick(this.props.id)}>Edit
                            </button>
                        </div>
                        <div className={"messageFooterObject"}>
                            <button className={"deleteButton"}
                                    onClick={() => this.props.deleteMessage(message.id)}>Delete
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = {
    showModal,
    setMessageId,
    deleteMessage
}

const mapStateToProps = (state: { chat: chatSystemState }) => {
    return {
        messages: state.chat.messages
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(OwnMessage);
